package org.tpachille.rssreader;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
//import org.omg.CORBA.PUBLIC_MEMBER;
//import org.omg.Messaging.SyncScopeHelper;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class RSSReader {

	public static void main(String[] args) throws ClientProtocolException, IOException, 
	ParserConfigurationException, SAXException{

		String url = "http://weblogs.java.net/pub/q/weblogs_rss";

		// ouvertuer d'un client
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(url);

		HttpResponse response= httpClient.execute(get);

		StatusLine statusline = response.getStatusLine();

		int statusCode = statusline.getStatusCode();
		if(statusCode != HttpStatus.SC_OK){
			System.out.println("Request failed  with code " + statusCode);
		}

		InputStream content = response.getEntity().getContent();

		DefaultHandler handler = new DefaultHandler(){


			public void startDocument(){
				System.out.println("D�but du document");
			}


			int profondeur = 0,  compteur1 = 0,	compteur2 = 0/*, itemSubElement =0*/;
			String rootElemment=null, theItem=null;
			boolean isTitle = false;
			List<String> theTitles = new ArrayList<String>();


			public void startElement(String uri, String localName,String qName, Attributes attributes){

				if(profondeur == 0){
					rootElemment=qName;
				}

				if(profondeur == 1){
					compteur1++;      // compte les �l�ments qui seront 
					// d�compt�s dans endElement()
				}
				profondeur++;

				if(qName.equals("item"))
				{
					//itemSubElement++;
					compteur2++;

					theItem = qName; // recup�ration du qName
				}

				// verification si l'item contient un titre 
				if ( theItem != null){
					if( qName.equals("title")){
						isTitle = true;
					}
				}

		};

		public void endElement(String uri, String localName, String qName){
			profondeur--;   // d�compte des �lements compt�s
			                // dans startElement();
			
			theItem = null;
			//System.out.println("Fin des �l�ments");
			
		}

		public void endDocument(){
			System.out.println(" �l�ment racine = "+rootElemment);
			System.out.println(" Nombre de sous �l�ment = "+ compteur1);
			System.out.println(" Nombre d' Item = "+ compteur2);
			
			System.out.println("");
			System.out.println("------------------------------------------");
			System.out.println("                les Titres                ");
			System.out.println("------------------------------------------");
			for (String s : theTitles){
				System.out.println(" "+ s);
			}
			
			System.out.println("Fin du document");
			
			
		}
		
		public void characters (char ch[], int start, int length){
             String contient = new String(ch, start, length);
			
             if(isTitle){
				theTitles.add(contient);
				isTitle = false;
			}
		}

	};

	SAXParser parser = SAXParserFactory.newInstance().newSAXParser();
	parser.parse(content,handler);
	

}
}
